(function ($) {

  Drupal.behaviors.zxcvbn = {
    attach: function (context) {
      window.Drupal.evaluatePasswordStrength = function (password, translate) {


        if (window.zxcvbn) {

          // Collect personal data
          var data = [];
          $('input:not(.password-field):not(.password-confirm)').each(function () {
            data.push($(this).val());
          });

          var res = zxcvbn(password, data);
          var text = res.crack_times_display.online_throttling_100_per_hour;
          var match = text.match(/^[\d]+/);
          if (match) {
            text = Drupal.t(text.replace(/^[\d]+/, '@t'), {'@t': match});
          }


        } else {
          console.error('zxcvbn library is not loaded');
        }
        var suggestionText = '<ul>';
        if (res.feedback.warning != "") {
          suggestionText += '<li>' + res.feedback.warning + '</li>';
        }
        if (res.feedback.suggestions.length > 0) {
          suggestionText += '<li>' + res.feedback.suggestions.join('</li><li>') + '</li>';
        }
        suggestionText += '</ul>';

        var indicatorClasses = {
          0: 'is-weak',
          1: 'is-weak',
          2: 'is-fair',
          3: 'is-good',
          4: 'is-strong'
        };

        return {
          strength: res.score * 25,
          message: suggestionText,
          indicatorText: text,
          indicatorClass: indicatorClasses[res.score]
        }
      };
    }
  };

})(jQuery);
